extern crate maildir;
extern crate argparse;

use argparse::{ArgumentParser, Store};

use maildir as md;
use std::path::Path;

pub fn main() {
    let mut md_dir = "/home/plum/.email/gmx".to_string();

    {
        let mut ap = ArgumentParser::new();

        ap.refer(&mut md_dir)
            .add_argument("Maildir", Store, "Maildir to deal with.");

        ap.parse_args_or_exit();
    }

    let my_box = md::Boxes::new(Path::new(&md_dir));
    let mut new_mails = match my_box.get_new() {
        Some(x) => x,
        None => panic!("No new mail"),
    };

    println!("New mails: {}", new_mails.len());

    for m in &mut new_mails {
        m.parse();
        if m.get_file_name().contains("KerbalStuff") {
            println!("Looking mail {}", m.get_file_name());
            // m.print();
            println!("{}", m);
        }
    }
}

