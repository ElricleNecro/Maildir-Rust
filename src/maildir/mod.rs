use std::fs;
use std::fs::DirEntry;
use std::path::Path;
use std::path::PathBuf;

use super::mail::Mail;

pub struct Maildir {
    name: String,
    new: PathBuf,
    cur: PathBuf,
    tmp: PathBuf,
}

impl Maildir {
    pub fn new(md: &Path) -> Maildir {
        let mut new = PathBuf::from(md);
        let mut tmp = PathBuf::from(md);
        let mut cur = PathBuf::from(md);

        new.push("new");
        if ! new.is_dir() {
            panic!("{} is not a directory.", new.display());
        }

        cur.push("cur");
        if ! cur.is_dir() {
            panic!("{} is not a directory.", cur.display());
        }

        tmp.push("tmp");
        if ! tmp.is_dir() {
            panic!("{} is not a directory.", tmp.display());
        }

        Maildir {
            name: md.file_name().unwrap().to_str().unwrap().to_string(),
            new: new,
            cur: cur,
            tmp: tmp,
        }
    }

    pub fn name(&self) -> String {
        self.name.clone()
    }

    pub fn get_new(&self) -> Option<Vec<Mail>> {
        let dir: Vec<DirEntry> = fs::read_dir(&self.new)
            .ok()
            .unwrap()
            .map(|x| x.ok().unwrap())
            .collect();

        if dir.len() == 0 {
            return None;
        }

        let mut res = Vec::new();

        for file in dir {
            if file.file_type().ok().unwrap().is_file() {
                let mut tmp = Mail::new(
                    file.path()
                );
                // tmp.parse();
                res.push(
                    tmp
                )
            }
        }

        return Some( res );
    }
}

