use regex::Regex;

use std::io;
use std::io::prelude::*;

use std::fs::File;

use std::path::Path;
use std::path::PathBuf;

use std::collections::HashMap;

use std::fmt::{Display,Formatter,Result as fmtResult};

use std::ops::Index;

pub struct Mail {
    file: PathBuf,
    fields: HashMap<String, Vec<String>>,
}

fn lines_from_file<P>(filename: P) -> Result<io::Lines<io::BufReader<File>>, io::Error> where P: AsRef<Path> {
    let file = try!(File::open(filename));
    Ok(io::BufReader::new(file).lines())
}

macro_rules! scanf {
    ( $string:expr, $sep:expr, $( $x:ty ),+ ) => {{
        let mut iter = $string.split($sep);
        ($(iter.next().and_then(|word| word.parse::<$x>().ok()),)*)
    }}
}

impl Mail {
    pub fn new(path: PathBuf) -> Mail {
        Mail { file: path, fields: HashMap::new() }
    }

    pub fn get_file_name(&self) -> String {
        self.file.to_str().unwrap().to_owned()
    }

    pub fn print(&mut self) {
        if self.fields.len() == 0 {
            self.parse();
        }

        println!("From: {}", self.fields.get("From").unwrap()[0]);
        println!("To: {}", self.fields.get("To").unwrap()[0]);
        println!("Body:\n {}", self.fields.get("Body").unwrap().join("\n"));
    }

    pub fn parse(&mut self) {
        let lines = match lines_from_file(&self.file) {
            Err(why) => panic!("Problem with file '{}': {:?}", self.file.display(), why),
            Ok(x) => x,
        };

        let motif = Regex::new(r"(?P<Fields>^[:graph:]+):\s*(?P<Value>.*)$").unwrap();
        let mut latest: String = String::new();
        let mut next_is_body = false;

        self.fields.insert(
            "Body".to_string(),
            Vec::new()
        );

        for l in lines {
            let line = l.unwrap();

            if line == "" {
                next_is_body = true;
            }

            if motif.is_match(&line) {
                let caps = motif.captures(&line).unwrap();
                latest = caps.name("Fields").unwrap().to_owned();

                if self.fields.contains_key(caps.name("Fields").unwrap()) {
                    self.fields.get_mut(caps.name("Fields").unwrap()).unwrap().push(
                            caps.name("Value").unwrap().to_owned()
                    )
                } else {
                    let mut val = Vec::new();
                    val.push(caps.name("Value").unwrap().to_owned());
                    self.fields.insert(
                        latest.clone(),
                        val
                    );
                }
            } else if next_is_body {
                self.fields.get_mut("Body").unwrap().push(
                    line.clone()
                );
            } else {
                self.fields.get_mut(&latest).unwrap().push(
                    line.clone()
                );
            }
        }

        if self.fields.get("Body").unwrap()[0] == "" {
            self.fields.get_mut("Body").unwrap().remove(0);
        }
    }

    pub fn get(&self, name: &str) -> Option<&Vec<String>> {
        if self.fields.contains_key(name) {
            Some(&self.fields[name])
        } else {
            None
        }
    }
}

impl Index<String> for Mail {
    type Output = Option<String>;

    fn index(&self, index: String) -> &Option<String> {
        if ! self.fields.contains_key(&index) {
            &None
        }

        match index {
            "Body" => &Some( self.fields[&index].join("\n") ),
            _ => &Some( self.fields[&index].join(" ") ),
        }
    }
}

impl Display for Mail {
    fn fmt(&self, f: &mut Formatter) -> fmtResult {
        // if self.fields.len() == 0 {
            // self.parse();
        // }

        write!(f, "From: {}\nTo: {}\nSubject:{}\n\n{}\n", self.fields.get("From").unwrap().join("\n"), self.fields.get("To").unwrap().join("\n"), self.fields.get("Subject").unwrap().join("\n"), self.fields.get("Body").unwrap().join("\n"))
    }
}
