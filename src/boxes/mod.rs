use std::fs;
use std::path::Path;

use std::collections::HashMap;

use super::Maildir;
use super::Mail;

pub struct Boxes {
    mailboxes: HashMap<String, Maildir>,
}

impl Boxes {
    pub fn new(md: &Path) -> Boxes {
        let mut out = Boxes { mailboxes: HashMap::new() };
        let boxes = match fs::read_dir(md) {
            Ok(p) => p,
            Err(err) => panic!(err),
        };

        for d in boxes {
            let act = d.ok().unwrap();
            if (! act.file_name().to_str().unwrap().starts_with(".") ) && act.file_type().ok().unwrap().is_dir() {
                let tmp = Maildir::new(&act.path());
                out.mailboxes.insert(
                    tmp.name(),
                    tmp
                );
            }
        }

        return out;
    }

    pub fn get_new(self) -> Option<Vec<Mail>> {
        let mut new: Vec<Mail> = Vec::new();

        for (_, m) in self.mailboxes {
            match m.get_new() {
                Some(x) => new.extend(x),
                None => continue,
            };
        }

        if new.len() == 0 {
            return None;
        }

        return Some(new);
    }

    pub fn get(&self, name: &str) -> Option<&Maildir> {
        if self.mailboxes.contains_key(name) {
            Some(&self.mailboxes[name])
        } else {
            None
        }
    }

    pub fn iter(&self) -> &HashMap<String, Maildir> {
        &self.mailboxes
    }
}
