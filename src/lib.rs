// #![crate_type="dylib"]

extern crate regex;

pub use self::maildir::Maildir;
pub use self::boxes::Boxes;
pub use self::mail::Mail;

mod maildir;
mod boxes;
mod mail;

