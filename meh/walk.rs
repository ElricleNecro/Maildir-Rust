use std::fs;
use std::path::Path;

pub fn test_walk_dir(p: &Path) {
    let dir = match fs::read_dir(p) {
        Ok(rdir) => rdir,
        Err(ss) => panic!(ss),
    };

    for d in dir {
        match d {
            Ok(a) => {
                println!("{:?}", a.path());
                match a.file_type() {
                    Err(x) => panic!(x),
                    Ok(x) => {
                        if x.is_dir() {
                            test_walk_dir(a.path().as_path());
                        }
                    }, //println!("{}", x.is_dir()),
                };
            },
            Err(a) => panic!(a),
        };
    }
}

pub fn main() {
    test_walk_dir(Path::new("/home/plum/.email"));
}
